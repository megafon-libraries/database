package database

import (
	"context"
	"database/sql"
	"errors"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

type Instance interface {
	Initializer
	Closer
	Executor
	ExecutorContext
	Transactions
	Informer
}

type Initializer interface {
	Init() *DB
}

type Closer interface {
	Close() error
}

//	Обрабатывает строку коннекта, может преобразовывать её
type ConnectExtractor interface {
	ConnectExtract(connect string) (string, error)
}

type SqlExecutor interface {
	Query(string, ...interface{}) (*sql.Rows, error)
	Exec(string, ...interface{}) (sql.Result, error)
}

type SqlExecutorContext interface {
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	ExecContext(context.Context, string, ...interface{}) (sql.Result, error)
}

type Informer interface {
	SetDatabaseInfo(name, value string)
	DatabaseInfo(name string) string
	DatabaseAllInfo() map[string]string
}

type Executor interface {
	Informer
	Query(string, ...interface{}) Result
	Exec(string, ...interface{}) ExecResult
}

type ExecutorContext interface {
	Informer
	QueryContext(context.Context, string, ...interface{}) Result
	ExecContext(context.Context, string, ...interface{}) ExecResult
}

type Transactions interface {
	Begin() (*Transaction, error)
}

type CustomConnector interface {
	CustomConnect(connect string) error
}

type CustomExecutor interface {
	CustomQuery(string, ...interface{}) Result
	CustomExec(string, ...interface{}) ExecResult
}

type CustomExecutorContext interface {
	CustomQueryContext(context.Context, string, ...interface{}) Result
	CustomExecContext(context.Context, string, ...interface{}) ExecResult
}

type ColumnTypeTransformer interface {
	ColumnTypeTransform(interface{}, string) interface{}
}

type ErrorFilterer interface {
	ErrorFilter(error) error
}

type DB struct {
	driver       string
	databaseInfo map[string]string
	parent       *Instance
	connect      string
	*sql.DB
}

func (db *DB) Init() *DB {
	db.databaseInfo = map[string]string{}
	return db
}

func (db *DB) OpenDB(driverName string, dataSourceName string) error {
	db.driver = driverName
	return db.FilterError(db.Connect(dataSourceName))
}

func (db *DB) Connect(connect string) error {
	var err error

	//	If parent have ConnectExtract function
	if db.parent != nil {
		if parent, ok := (*db.parent).(ConnectExtractor); ok {
			if data, err := parent.ConnectExtract(connect); err == nil {
				connect = data
			}
		}
	}

	db.connect = connect

	//	If parent have CustomConnect function
	if db.parent != nil {
		if parent, ok := (*db.parent).(CustomConnector); ok {
			return db.FilterError(parent.CustomConnect(connect))
		}
	}

	db.DB, err = sql.Open(db.driver, connect)
	if err == nil {
		err = db.DB.Ping()
	}
	return db.FilterError(err)
}

func (db *DB) SetDriver(driver string) {
	db.driver = driver
}

func (db *DB) SetDatabaseInfo(name, value string) {
	if db.databaseInfo == nil {
		db.databaseInfo = map[string]string{}
	}
	db.databaseInfo[name] = value
}

func (db *DB) DatabaseInfo(name string) string {
	return db.databaseInfo[name]
}

func (db *DB) DatabaseAllInfo() map[string]string {
	return db.databaseInfo
}

func (db *DB) SetParent(e Instance) {
	db.parent = &e
}

func (db *DB) Parent() *Instance {
	return db.parent
}

func (db *DB) FilterError(err error) error {
	if db.parent != nil {
		if parent, ok := (*db.parent).(ErrorFilterer); ok {
			return parent.ErrorFilter(err)
		}
	}
	return err
}

func (db *DB) Close() error {
	//	If parent have Close method
	if db.parent != nil {
		if parent, ok := (*db.parent).(Closer); ok {
			return parent.Close()
		}
	}
	return db.DB.Close()
}

// Convert named bind variables to numeric
func (db *DB) NamedToOrdered(query string, namedParams map[string]interface{}) (string, []interface{}) {
	query += " "
	keys := "(\\:(?:"
	indexes := make([]string, 0, len(namedParams))
	for key := range namedParams {
		indexes = append(indexes, key[1:])
	}
	keys += strings.Join(indexes, "|") + "){1})([^-a-zA-Z0-9_]{1})"
	searcher := regexp.MustCompile(keys)
	data := searcher.FindAllStringSubmatch(query, -1)
	var index int64 = 1
	result := make([]interface{}, 0, len(data))
	for _, value := range data {
		query = strings.Replace(query, value[0], ":"+strconv.FormatInt(index, 10)+value[2], 1)
		result = append(result, namedParams[value[1]])
		index++
	}
	return query[0 : len(query)-1], result
}

func (db *DB) Begin() (*Transaction, error) {
	var transaction Transaction
	trx, err := db.DB.Begin()
	if err == nil {
		transaction = Transaction{
			Tx: trx,
			DB: db,
		}
	}
	return &transaction, err
}

func (db *DB) Query(query string, params ...interface{}) Result {
	ctx := context.Background()
	return db.QueryContext(ctx, query, params...)
}

func (db *DB) QueryContext(ctx context.Context, query string, params ...interface{}) Result {
	return db.queryContext(ctx, db.DB, query, params...)
}

func (db *DB) queryContext(ctx context.Context, src SqlExecutorContext, query string, params ...interface{}) Result {
	var (
		rows *sql.Rows
		err  error
	)
	result := Result{Headers: make([]TableHeader, 0), Rows: make([]TableData, 0)}

	//	Если у родительского объекта присуствует метод CustomQueryContext, вызываем его
	//	Подготовка параметров при этом не производится
	if db.parent != nil {
		if parent, ok := (*db.parent).(CustomExecutorContext); ok {
			return parent.CustomQueryContext(ctx, query, params...)
		} else if parent, ok := (*db.parent).(CustomExecutor); ok {
			return parent.CustomQuery(query, params...)
		}
	}

	query, params = db.PrepareParams(query, params...)
	rows, err = src.QueryContext(ctx, query, params...)
	if err != nil {
		result.Error = db.FilterError(errors.New("error running query: " + err.Error()))
		return result
	}

	return db.ParseRows(rows)
}

func (db *DB) Exec(query string, params ...interface{}) ExecResult {
	ctx := context.Background()
	return db.ExecContext(ctx, query, params...)
}

func (db *DB) ExecContext(ctx context.Context, query string, params ...interface{}) ExecResult {
	return db.execContext(ctx, db.DB, query, params...)
}

func (db *DB) execContext(ctx context.Context, src SqlExecutorContext, query string, params ...interface{}) ExecResult {
	query, params = db.PrepareParams(query, params...)

	//	Execute query
	//	If parent have Query method
	if db.parent != nil {
		if parent, ok := (*db.parent).(CustomExecutorContext); ok {
			return parent.CustomExecContext(ctx, query, params...)
		} else if parent, ok := (*db.parent).(CustomExecutor); ok {
			return parent.CustomExec(query, params...)
		}
	}
	var err error
	result := ExecResult{}
	result.Result, err = src.ExecContext(ctx, query, params...)
	if err != nil {
		result.Error = db.FilterError(errors.New("error running query: " + err.Error()))
	}

	return result
}

//	Replace named bind variables to numeric
func (db *DB) PrepareParams(query string, params ...interface{}) (string, []interface{}) {
	if len(params) == 1 && reflect.TypeOf(params[0]) == reflect.TypeOf(map[string]interface{}{}) {
		query, params = db.NamedToOrdered(query, params[0].(map[string]interface{}))
	}

	if len(params) == 1 && params[0] == nil {
		params = nil
	}
	return query, params
}

func (db *DB) ParseRows(rows *sql.Rows) Result {
	defer func() {
		_ = rows.Close()
	}()

	var (
		columnNames []string
		types       []*sql.ColumnType
		err         error
	)

	result := Result{Headers: make([]TableHeader, 0), Rows: make([]TableData, 0)}

	//	Check data in result
	if columnNames, err = rows.Columns(); err != nil {
		result.Error = db.FilterError(errors.New("incorrect columns, reason: " + err.Error()))
		return result
	}

	//	Create columns slice (name, type, size and nullable)
	if types, err = rows.ColumnTypes(); err != nil {
		result.Error = db.FilterError(errors.New("incorrect columns info: " + err.Error()))
		return result
	}

	//	Set headers data
	for _, s := range types {
		header := TableHeader{Name: s.Name(), Type: s.DatabaseTypeName()}

		if length, ok := s.Length(); ok {
			header.Size = &length
		}
		if nullable, ok := s.Nullable(); ok {
			header.Nullable = &nullable
		}
		result.Headers = append(result.Headers, header)
	}

	//	Prepare template for row data
	cols := make([]interface{}, len(columnNames))
	colPtr := make([]interface{}, len(columnNames))
	for i := range columnNames {
		colPtr[i] = &cols[i]
	}

	for rows.Next() {
		if err = rows.Scan(colPtr...); err != nil {
			result.Error = db.FilterError(errors.New("incorrect result: " + err.Error()))
			return result
		}
		data := TableData{Data: make(map[string]interface{})}
		for i, col := range cols {
			if db.parent != nil {
				if parent, ok := (*db.parent).(ColumnTypeTransformer); ok {
					data.Data[columnNames[i]] = parent.ColumnTypeTransform(col, result.Headers[i].Type)
					continue
				}
			}
			data.Data[columnNames[i]] = col
		}
		result.Rows = append(result.Rows, data)
	}

	return result
}
