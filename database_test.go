package database

import (
	"bou.ke/monkey"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
	"reflect"
	"testing"
)

var db = DB{}

type sqliteTestDB struct {
	DB
}

func (db *sqliteTestDB) Init() *DB {
	db.SetDriver("sqlite3")
	db.SetParent(db)
	return &db.DB
}

func (db *sqliteTestDB) ConnectExtract(_ string) (string, error) {
	return "test/sqlite.db", nil
}

func (db *sqliteTestDB) Connect(connect string) error {
	db.Init()
	return db.DB.Connect(connect)
}

func (db *sqliteTestDB) ErrorFilter(err error) error {
	return err
}

func (db *sqliteTestDB) ColumnTypeTransform(col interface{}, _ string) interface{} {
	return col
}

func OpenDB(t *testing.T) {
	db.SetDriver("sqlite3")
	if err := db.Connect("test/sqlite.db"); err != nil {
		t.Fatalf("Fail connect: %s", err)
	}
}

func TestDB_Init(t *testing.T) {
	db2 := db.Init()
	if db2 != &db {
		t.Fatalf("Invalid init function")
	}
	//if (*DB.Parent()).(DB) != DB {
	//	t.Fatalf("Invalid init function")
	//}
}

func TestDB_Open(t *testing.T) {
	base := DB{}
	if err := base.OpenDB("unknown", "test/sqlite.db"); err == nil {
		t.Fatalf("Invalid driver processing")
	}
	if err := base.OpenDB("sqlite3", "test/sqlite.db"); err != nil {
		t.Fatalf("Invalid open: %s", err)
	}
}

func TestDB_Connect(t *testing.T) {
	base := DB{}
	if err := base.Connect("not allow"); err == nil {
		t.Fatalf("Fail invalid connect")
	}
}

func TestDB_Close(t *testing.T) {
	OpenDB(t)
	if err := db.Close(); err != nil {
		t.Fatalf("Fail connect: %s", err)
	}
}

func TestDB_DatabaseInfo(t *testing.T) {
	OpenDB(t)
	db.databaseInfo = nil
	db.SetDatabaseInfo("test", "passed")
	require.Equal(t, "passed", db.DatabaseInfo("test"))
	require.Equal(t, map[string]string{"test": "passed"}, db.DatabaseAllInfo())
}

func TestDB_Parent(t *testing.T) {
	test := &sqliteTestDB{}
	db := test.Init()
	require.Equal(t, test, (*db.Parent()).(*sqliteTestDB), "Invalid parent")
}

func TestDB_NamedToOrdered(t *testing.T) {
	var (
		resultQuery string
		resultData  []interface{}
	)

	//	Incorrect bind variable (with numeric suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data2", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :data2", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: bind variable (with numeric suffix)")

	//	Incorrect bind variable (with text suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :dataset", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :dataset", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: bind variable (with text suffix)")

	//	Incorrect bind variable (with symbol suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data_", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :data_", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: bind variable (with symbol suffix)")

	//	Incorrect bind variable (with symbol and numeric suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data_2", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :data_2", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification:  bind variable (with symbol and numeric suffix)")

	//	Incorrect bind variable (with symbol and text suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data_info", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :data_info", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: bind variable (with symbol and text suffix)")

	//	Incorrect bind variable (short name)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :dat", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :dat", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: bind variable (short name)")

	//	Normal, bind variable in end
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :1", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{3}, resultData, "Incorrect query modification: bind variable in end")

	//	Normal, bind variable in middle (with space as suffix)
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data order by 1", map[string]interface{}{":data": 3})
	require.Equal(t, "select * from data where data.one = :1 order by 1", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{3}, resultData, "Incorrect query modification: bind variable in middle (with space as suffix)")

	//	Normal, mixed bind variable, extra data in variable
	resultQuery, resultData = db.NamedToOrdered("select * from data where data.one = :data", map[string]interface{}{":dataset": 5, ":data": 3})
	require.Equal(t, "select * from data where data.one = :1", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{3}, resultData, "Incorrect query modification: mixed bind variable, extra data in variable")

	//	Normal, no bind variable, empty data
	resultQuery, resultData = db.NamedToOrdered("select * from data", map[string]interface{}{})
	require.Equal(t, "select * from data", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: no bind variable, empty data")

	//	Normal, no bind variable, nil data
	resultQuery, resultData = db.NamedToOrdered("select * from data", nil)
	require.Equal(t, "select * from data", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: no bind variable, nil data")

	//	Normal, no bind variable, extra data in variable
	resultQuery, resultData = db.NamedToOrdered("select * from data", map[string]interface{}{":dataset": 5, ":data": 3})
	require.Equal(t, "select * from data", resultQuery, "Incorrect query modification")
	require.Equal(t, []interface{}{}, resultData, "Incorrect query modification: no bind variable, extra data in variable")
}

func TestDB_Query(t *testing.T) {
	OpenDB(t)

	//	Test invalid query
	result := db.Query("SELECT timeline,cnt,master,slave from zzz")
	require.Error(t, result.Error, "Fail test on invalid query")

	//	Test invalid columns
	var guard *monkey.PatchGuard
	var rows *sql.Rows
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(rows), "Columns", func(rs *sql.Rows) ([]string, error) {
		return make([]string, 0), errors.New("invalid column test passed")
	})
	result = db.Query("SELECT timeline from calls")
	require.Error(t, result.Error, "Fail test on invalid columns")
	guard.Unpatch()

	//	Test invalid column type
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(rows), "ColumnTypes", func(rs *sql.Rows) ([]*sql.ColumnType, error) {
		types := make([]*sql.ColumnType, 0)
		return types, errors.New("invalid column type test passed")
	})
	result = db.Query("SELECT timeline from calls")
	require.Error(t, result.Error, "Fail test on invalid column type")
	guard.Unpatch()

	//	Test invalid row scan
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(rows), "Scan", func(rs *sql.Rows, dest ...interface{}) error {
		return errors.New("invalid row scan test passed")
	})
	result = db.Query("SELECT timeline from calls")
	require.Error(t, result.Error, "Fail test on invalid row scan")
	guard.Unpatch()

	result = db.Query("SELECT timeline,cnt,txt,boo from calls", nil)
	require.NoError(t, result.Error, "Fail connect:", result.Error)
	k, _ := json.Marshal(result)
	example := "{\"headers\":[{\"name\":\"timeline\",\"type\":\"date\",\"size\":null,\"nullable\":true},{\"name\":\"cnt\"," +
		"\"type\":\"integer\",\"size\":null,\"nullable\":true},{\"name\":\"txt\",\"type\":\"text\",\"size\":null,\"nullable\":true}," +
		"{\"name\":\"boo\",\"type\":\"bool\",\"size\":null,\"nullable\":true}],\"rows\":[{\"data\":{\"boo\":null,\"cnt\":9574," +
		"\"timeline\":\"2020-01-01T00:00:00Z\",\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-02T00:00:00Z\"," +
		"\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"cnt\":9549,\"timeline\":\"2020-01-03T00:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9715,\"timeline\":\"2020-01-04T00:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801," +
		"\"timeline\":\"2020-01-05T00:00:00Z\",\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"cnt\":9175,\"timeline\":\"2020-01-06T00:00:00Z\"," +
		"\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null,\"cnt\":11084,\"timeline\":\"2020-01-07T00:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9499,\"timeline\":\"2020-01-08T00:00:00Z\",\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"cnt\":8512," +
		"\"timeline\":\"2020-01-09T00:00:00Z\",\"txt\":\"ROW9\"}}]}"
	require.Equal(t, example, string(k), "Incorrect query modification")

	data := sqliteTestDB{}
	_ = data.Connect("")
	result = data.Query("SELECT timeline,cnt,txt,boo from calls", map[string]interface{}{})
	require.NoError(t, result.Error, "Fail connect:", result.Error)
	k, _ = json.Marshal(result)
	example = "{\"headers\":[{\"name\":\"timeline\",\"type\":\"date\",\"size\":null,\"nullable\":true},{\"name\":\"cnt\"," +
		"\"type\":\"integer\",\"size\":null,\"nullable\":true},{\"name\":\"txt\",\"type\":\"text\",\"size\":null,\"nullable\":true}," +
		"{\"name\":\"boo\",\"type\":\"bool\",\"size\":null,\"nullable\":true}],\"rows\":[{\"data\":{\"boo\":null,\"cnt\":9574," +
		"\"timeline\":\"2020-01-01T00:00:00Z\",\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-02T00:00:00Z\"," +
		"\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"cnt\":9549,\"timeline\":\"2020-01-03T00:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9715,\"timeline\":\"2020-01-04T00:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801," +
		"\"timeline\":\"2020-01-05T00:00:00Z\",\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"cnt\":9175,\"timeline\":\"2020-01-06T00:00:00Z\"," +
		"\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null,\"cnt\":11084,\"timeline\":\"2020-01-07T00:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9499,\"timeline\":\"2020-01-08T00:00:00Z\",\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"cnt\":8512," +
		"\"timeline\":\"2020-01-09T00:00:00Z\",\"txt\":\"ROW9\"}}]}"
	require.Equal(t, example, string(k), "Incorrect query modification")
}

func TestDB_CustomQuery(t *testing.T) {
	dataCustom := sqliteTestCustomDB{}
	_ = dataCustom.Init()
	_ = dataCustom.Connect("")
	result := dataCustom.Query("any query", map[string]interface{}{})
	require.NoError(t, result.Error, "Fail connect:", result.Error)
	exec := dataCustom.Exec("any exec", map[string]interface{}{})
	require.NoError(t, exec.Error, "Fail connect:", exec.Error)
}

func TestDB_CustomQueryContext(t *testing.T) {
	dataCustom := sqliteTestCustomContextDB{}
	_ = dataCustom.Init()
	_ = dataCustom.Connect("")
	result := dataCustom.Query("any query", map[string]interface{}{})
	require.NoError(t, result.Error, "Fail connect:", result.Error)
	exec := dataCustom.Exec("any exec", map[string]interface{}{})
	require.NoError(t, exec.Error, "Fail connect:", exec.Error)
}

type sqliteTestCustomDB struct {
	DB
}

func (db *sqliteTestCustomDB) Init() *DB {
	db.SetDriver("sqlite3")
	db.SetParent(db)
	return &db.DB
}
func (db *sqliteTestCustomDB) CustomQuery(_ string, _ ...interface{}) Result {
	result := Result{
		Headers: make([]TableHeader, 0),
		Rows:    make([]TableData, 0),
		Error:   nil,
	}
	return result
}
func (db *sqliteTestCustomDB) CustomExec(_ string, _ ...interface{}) ExecResult {
	result := ExecResult{}
	return result
}

type sqliteTestCustomContextDB struct {
	DB
}

func (db *sqliteTestCustomContextDB) Init() *DB {
	db.SetDriver("sqlite3")
	db.SetParent(db)
	return &db.DB
}
func (db *sqliteTestCustomContextDB) CustomQueryContext(_ context.Context, _ string, _ ...interface{}) Result {
	result := Result{
		Headers: make([]TableHeader, 0),
		Rows:    make([]TableData, 0),
		Error:   nil,
	}
	return result
}
func (db *sqliteTestCustomContextDB) CustomExecContext(_ context.Context, _ string, _ ...interface{}) ExecResult {
	result := ExecResult{}
	return result
}
