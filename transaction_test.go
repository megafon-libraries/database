package database

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestTransaction_Exec(t *testing.T) {
	data := sqliteTestDB{}
	_ = data.Connect("")

	trx, err := data.Begin()
	require.NoError(t, err, "Fail open transaction, reason: ", err)

	queryResult := trx.Query("select count(*) CNT from calls")
	require.NoError(t, queryResult.Error, "Fail count records: ", err)
	require.Equal(t, int64(9), queryResult.Rows[0].Data["CNT"], "Invalid records found")

	execResult := trx.Exec("insert into calls(timeline, cnt, txt, boo) values (date('now'), 100, 'data', false)")
	require.NoError(t, execResult.Error, "Fail insert record: ", err)
	rws, err := execResult.RowsAffected()
	require.NoError(t, err, "Fail insert record: ", err)
	require.Equal(t, int64(1), rws, "Fail insert record: ", err)

	queryResult = trx.Query("select count(*) CNT from calls")
	require.NoError(t, queryResult.Error, "Fail count records: ", err)
	require.Equal(t, int64(10), queryResult.Rows[0].Data["CNT"], "Invalid records found")

	err = trx.Rollback()
	require.NoError(t, err, "Fail close transaction: ", err)

}
