package database

import (
	"context"
	"database/sql"
)

type Transaction struct {
	*sql.Tx
	DB *DB
}

func (trx *Transaction) Query(query string, params ...interface{}) Result {
	ctx := context.Background()
	return trx.QueryContext(ctx, query, params...)
}

func (trx *Transaction) QueryContext(ctx context.Context, query string, params ...interface{}) Result {
	return trx.DB.queryContext(ctx, trx.Tx, query, params...)
}

func (trx *Transaction) Exec(query string, params ...interface{}) ExecResult {
	ctx := context.Background()
	return trx.ExecContext(ctx, query, params...)
}

func (trx *Transaction) ExecContext(ctx context.Context, query string, params ...interface{}) ExecResult {
	return trx.DB.execContext(ctx, trx.Tx, query, params...)
}

// Database Info section

func (trx *Transaction) SetDatabaseInfo(name, value string) {
	trx.DB.SetDatabaseInfo(name, value)
}

func (trx *Transaction) DatabaseInfo(name string) string {
	return trx.DB.DatabaseInfo(name)
}

func (trx *Transaction) DatabaseAllInfo() map[string]string {
	return trx.DB.DatabaseAllInfo()
}
