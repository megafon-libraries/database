package database

import (
	"context"
	"errors"
	"sync"
)

type Manager struct {
	Pool   map[string]ManagerPool
	update *sync.RWMutex
}

func (mgr *Manager) AddDatabase(instance Instance, connectionString string, sessionLimit int, messagesLimit int) (*ManagerPool, error) {
	if mgr.Pool == nil {
		mgr.Pool = map[string]ManagerPool{}
		mgr.update = &sync.RWMutex{}
	}
	databaseName := connectionString
	if _, ok := mgr.Pool[databaseName]; ok {
		return nil, errors.New("database already added: " + databaseName)
	}
	db := instance.Init()
	if connectionString != "" || db.Ping() != nil {
		if err := db.Connect(connectionString); err != nil {
			return nil, errors.New("fail to connect to database: " + err.Error())
		}
	}
	pool := ManagerPool{
		update:       &sync.RWMutex{},
		wg:           &sync.WaitGroup{},
		db:           db,
		session:      make([]session, sessionLimit),
		transactions: map[int]chan message{},
		messages:     make(chan message, messagesLimit),
	}

	mgr.update.Lock()
	defer mgr.update.Unlock()

	sessions := mgr.Pool[databaseName].session
	for i := 0; i < sessionLimit; i++ {
		ctx := context.Background()
		conn, err := db.Conn(ctx)
		if err != nil {
			for _, value := range mgr.Pool[databaseName].session {
				value.ctx.Done()
			}
			pool.wg.Wait()
			close(pool.messages)
			return nil, err
		}
		newSession := session{ctx: ctx, conn: conn}
		sessions = append(sessions, newSession)
		go pool.worker(&newSession, i)
	}
	mgr.Pool[databaseName] = pool
	return &pool, nil
}

func (mgr *Manager) QueryBatch(poolName string, query []string, params [][]interface{}) Result {
	mgr.update.RLock()
	pool, ok := mgr.Pool[poolName]
	mgr.update.RUnlock()
	if ok {
		return pool.QueryBatch(query, params)
	}
	return Result{Error: errors.New("unknown pool name")}
}

func (mgr *Manager) Query(poolName string, query string, params ...interface{}) Result {
	mgr.update.RLock()
	pool, ok := mgr.Pool[poolName]
	mgr.update.RUnlock()
	if ok {
		return pool.Query(query, params)
	}
	return Result{Error: errors.New("unknown pool name")}
}
