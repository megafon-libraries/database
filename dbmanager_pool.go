package database

import (
	"context"
	"database/sql"
	"sync"
)

type ManagerPool struct {
	update       *sync.RWMutex
	wg           *sync.WaitGroup
	db           *DB
	session      []session
	transactions map[int]chan message
	messages     chan message
}

type message struct {
	query  []string
	params [][]interface{}
	cmd    string
	result chan Result
}

type session struct {
	ctx      context.Context
	conn     *sql.Conn
}

type TransactionManager struct {
	messages     chan message
}

func (tx *TransactionManager) QueryBatch(query []string, params [][]interface{}) Result {
	result := make(chan Result)
	message := message{
		query:  query,
		params: params,
		cmd:    "",
		result: result,
	}
	tx.messages <- message
	data := <-result
	close(result)
	return data
}

func (tx *TransactionManager) Query(query string, params ...interface{}) Result {
	result := make(chan Result)
	message := message{
		query:  []string{query},
		params: [][]interface{}{params},
		cmd:    "",
		result: result,
	}
	tx.messages <- message
	data := <-result
	close(result)
	return data
}

func (pool *ManagerPool) QueryBatch(query []string, params [][]interface{}) Result {
	result := make(chan Result)
	message := message{
		query:  query,
		params: params,
		cmd:    "",
		result: result,
	}
	pool.messages <- message
	data := <-result
	close(result)
	return data
}

func (pool *ManagerPool) Query(query string, params ...interface{}) Result {
	result := make(chan Result)
	message := message{
		query:  []string{query},
		params: [][]interface{}{params},
		cmd:    "",
		result: result,
	}
	pool.messages <- message
	data := <-result
	close(result)
	return data
}

func (pool *ManagerPool) worker(data *session, cnt int) {
	pool.wg.Add(1)
	defer func() {
		_ = data.conn.Close()
		pool.wg.Done()
	}()

	var (
		transaction bool
	)
	messages := pool.messages
	for {
		select {
		case <-data.ctx.Done():
			return
		case msg, ok := <-messages:
			if ok && msg.cmd == "transaction begin" && !transaction {
				messages = make(chan message)
				pool.transactionOpen(messages, cnt)
				transaction = true
			} else if (ok && msg.cmd == "transaction end" && transaction) || (!ok && transaction) {
				messages = pool.messages
				pool.transactionClose(cnt)
				transaction = false
			} else if !ok {
				return
			}
			for index, queryText := range msg.query {
				var (
					rows   *sql.Rows
					result Result
					err    error
				)
				query, params := pool.db.PrepareParams(queryText, msg.params[index]...)
				if rows, err = data.conn.QueryContext(data.ctx, query, params...); err == nil {
					result = pool.db.ParseRows(rows)
				} else {
					result.Error = err
				}
				msg.result <- result
			}
		}
	}
}

func (pool *ManagerPool) transactionOpen(msg chan message, cnt int) {
	_, ok := pool.transactions[cnt]
	if ok {
		pool.transactionClose(cnt)
	}
	pool.transactions[cnt] = msg
}

func (pool *ManagerPool) transactionClose(cnt int) {
	delete(pool.transactions, cnt)
}
