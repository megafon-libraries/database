package database

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"testing"
)

func fillData(t *testing.T) Result {
	var err error
	data := Result{}
	example := []byte("{\"headers\":[{\"name\":\"timeline\",\"type\":\"DATE\",\"size\":32,\"nullable\":true},{\"name\":\"cnt\"," +
		"\"type\":\"NUMBER\",\"size\":32,\"nullable\":true},{\"name\":\"txt\",\"type\":\"VARCHAR2\",\"size\":32,\"nullable\":true}," +
		"{\"name\":\"boo\",\"type\":\"nullbool\",\"size\":32,\"nullable\":true}],\"rows\":[{\"data\":{\"boo\":null,\"cnt\":9575," +
		"\"timeline\":\"2020-01-01T03:00:00Z\",\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-02T03:00:00Z\"," +
		"\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"cnt\":9549,\"timeline\":\"2020-01-03T03:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9715,\"timeline\":\"2020-01-04T03:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801,\"timeline\":\"2020-01-05T03:00:00Z\"," +
		"\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"cnt\":9175,\"timeline\":\"2020-01-06T03:00:00Z\",\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":11084,\"timeline\":\"2020-01-07T03:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-08T03:00:00Z\"," +
		"\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"cnt\":8512,\"timeline\":\"2020-01-09T03:00:00Z\",\"txt\":\"ROW9\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9575,\"timeline\":\"2020-01-01T03:00:00Z\",\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-02T03:00:00Z\"," +
		"\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"cnt\":9549,\"timeline\":\"2020-01-03T03:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9715,\"timeline\":\"2020-01-04T03:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801,\"timeline\":\"2020-01-05T03:00:00Z\"," +
		"\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"cnt\":9175,\"timeline\":\"2020-01-06T03:00:00Z\",\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":11084,\"timeline\":\"2020-01-07T03:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"timeline\":\"2020-01-08T03:00:00Z\"," +
		"\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"cnt\":8512,\"timeline\":\"2020-01-09T03:00:00Z\",\"txt\":\"ROW9\"}}]}")
	if err = json.Unmarshal(example, &data); err != nil {
		t.Fatalf("Fail unmarshal test data")
	}
	if k, err := json.Marshal(data); err != nil {
		t.Fatalf("Fail to marshal unmarshalled test data")
	} else {
		require.Equal(t, k, example, "Incorrect query conversation")
	}
	return data
}
func TestResult_RemoveColumn(t *testing.T) {
	example := []byte("{\"headers\":[{\"name\":\"cnt\",\"type\":\"NUMBER\",\"size\":32,\"nullable\":true},{\"name\":\"txt\"," +
		"\"type\":\"VARCHAR2\",\"size\":32,\"nullable\":true},{\"name\":\"boo\",\"type\":\"nullbool\",\"size\":32,\"nullable\":true}]," +
		"\"rows\":[{\"data\":{\"boo\":null,\"cnt\":9575,\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9549,\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null,\"cnt\":9715,\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801,\"txt\":\"ROW5\"}}," +
		"{\"data\":{\"boo\":null,\"cnt\":9175,\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null,\"cnt\":11084,\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9499,\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"cnt\":8512,\"txt\":\"ROW9\"}},{\"data\":{\"boo\":null,\"cnt\":9575,\"txt\":\"ROW1\"}}," +
		"{\"data\":{\"boo\":null,\"cnt\":9499,\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"cnt\":9549,\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":9715,\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"cnt\":9801,\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"cnt\":9175,\"txt\":\"ROW6\"}}," +
		"{\"data\":{\"boo\":null,\"cnt\":11084,\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null,\"cnt\":9499,\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null," +
		"\"cnt\":8512,\"txt\":\"ROW9\"}}]}")
	result := fillData(t)
	result.RemoveColumn("timeline")
	k, _ := json.Marshal(result)
	require.Equal(t, k, example, "Incorrect column remove")
}

func TestResult_RenameColumn(t *testing.T) {
	example := []byte("{\"headers\":[{\"name\":\"timeline\",\"type\":\"DATE\",\"size\":32,\"nullable\":true},{\"name\":\"mazeltov\"," +
		"\"type\":\"NUMBER\",\"size\":32,\"nullable\":true},{\"name\":\"txt\",\"type\":\"VARCHAR2\",\"size\":32,\"nullable\":true}," +
		"{\"name\":\"boo\",\"type\":\"nullbool\",\"size\":32,\"nullable\":true}],\"rows\":[{\"data\":{\"boo\":null,\"mazeltov\":9575," +
		"\"timeline\":\"2020-01-01T03:00:00Z\",\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"mazeltov\":9499,\"timeline\":\"2020-01-02T03:00:00Z\"," +
		"\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null,\"mazeltov\":9549,\"timeline\":\"2020-01-03T03:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null," +
		"\"mazeltov\":9715,\"timeline\":\"2020-01-04T03:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"mazeltov\":9801," +
		"\"timeline\":\"2020-01-05T03:00:00Z\",\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"mazeltov\":9175,\"timeline\":\"2020-01-06T03:00:00Z\"," +
		"\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null,\"mazeltov\":11084,\"timeline\":\"2020-01-07T03:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null," +
		"\"mazeltov\":9499,\"timeline\":\"2020-01-08T03:00:00Z\",\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"mazeltov\":8512," +
		"\"timeline\":\"2020-01-09T03:00:00Z\",\"txt\":\"ROW9\"}},{\"data\":{\"boo\":null,\"mazeltov\":9575,\"timeline\":\"2020-01-01T03:00:00Z\"," +
		"\"txt\":\"ROW1\"}},{\"data\":{\"boo\":null,\"mazeltov\":9499,\"timeline\":\"2020-01-02T03:00:00Z\",\"txt\":\"ROW2\"}},{\"data\":{\"boo\":null," +
		"\"mazeltov\":9549,\"timeline\":\"2020-01-03T03:00:00Z\",\"txt\":\"ROW3\"}},{\"data\":{\"boo\":null,\"mazeltov\":9715," +
		"\"timeline\":\"2020-01-04T03:00:00Z\",\"txt\":\"ROW4\"}},{\"data\":{\"boo\":null,\"mazeltov\":9801,\"timeline\":\"2020-01-05T03:00:00Z\"," +
		"\"txt\":\"ROW5\"}},{\"data\":{\"boo\":null,\"mazeltov\":9175,\"timeline\":\"2020-01-06T03:00:00Z\",\"txt\":\"ROW6\"}},{\"data\":{\"boo\":null," +
		"\"mazeltov\":11084,\"timeline\":\"2020-01-07T03:00:00Z\",\"txt\":\"ROW7\"}},{\"data\":{\"boo\":null,\"mazeltov\":9499," +
		"\"timeline\":\"2020-01-08T03:00:00Z\",\"txt\":\"ROW8\"}},{\"data\":{\"boo\":null,\"mazeltov\":8512,\"timeline\":\"2020-01-09T03:00:00Z\"," +
		"\"txt\":\"ROW9\"}}]}")
	result := fillData(t)
	result.RenameColumn("cnt", "mazeltov")
	k, _ := json.Marshal(result)
	require.Equal(t, k, example, "Incorrect column rename")
}
