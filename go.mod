module gitlab.com/megafon-libraries/database

go 1.14

require (
	bou.ke/monkey v1.0.2
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/stretchr/testify v1.6.1
)
