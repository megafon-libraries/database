package database

import (
	"sync"
	"time"
)

type Box struct {
	Pool   map[string]*BoxElement
	update *sync.RWMutex
}

type BoxElement struct {
	db            *DB
	status        bool
	checkInterval int
	close         chan bool
}

func (b *Box) New() {
	if b.Pool != nil {
		return
	}
	b.Pool = map[string]*BoxElement{}
	b.update = &sync.RWMutex{}
}

func (b *Box) Add(instance Instance, connectionString string, sessionLimit int, checkerIntervalInSecond int) (*DB, error) {
	b.New()
	databaseName := connectionString
	db := b.Database(databaseName)
	if db != nil {
		return db, nil
	}

	db = instance.Init()
	if err := db.Connect(connectionString); err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(sessionLimit)

	newElement := BoxElement{db: db, status: true, close: make(chan bool, 0), checkInterval: checkerIntervalInSecond}
	if checkerIntervalInSecond > 0 {
		go newElement.statusChecker()
	} else {
		newElement.close = nil
	}
	b.update.Lock()
	b.Pool[databaseName] = &newElement
	b.update.Unlock()

	return db, nil
}

func (b *Box) Delete(connectionString string) {
	b.New()
	b.update.Lock()
	if element, ok := b.Pool[connectionString]; ok {
		if element.close != nil {
			element.close <- true
		}
		delete(b.Pool, connectionString)
	}
	b.update.Unlock()
}

func (b *Box) Element(connectionString string) *BoxElement {
	b.New()
	b.update.RLock()
	element, _ := b.Pool[connectionString]
	b.update.RUnlock()
	return element
}

func (b *Box) Database(connectionString string) *DB {
	b.New()
	element := b.Element(connectionString)
	if element == nil {
		return nil
	}
	return element.db
}

func (b *Box) Status(connectionString string) bool {
	b.New()
	element := b.Element(connectionString)
	if element == nil {
		return false
	}
	return element.status
}

func (e *BoxElement) statusChecker() {
	for {
		select {
		case <-e.close:
			return
		case <-time.After(time.Second * time.Duration(e.checkInterval)):
			err := e.db.Ping()
			e.status = err == nil
		}
	}
}
