package database

import (
	"context"
	"errors"
	"sync"
	"time"
)

const (
	RotateRoundRobin = iota
	RotateUseLast
	RotateOnFail
)

type Balancer struct {
	DB
	sync.RWMutex
	sync.WaitGroup
	init     bool
	db       []*DB
	state    []bool
	last     int
	rotation int
	timeout  int
	close    chan bool
}

func (blc *Balancer) Init() *DB {
	blc.Lock()
	if !blc.init {
		blc.db = make([]*DB, 0)
		blc.state = make([]bool, 0)
		blc.SetParent(blc)
		blc.close = make(chan bool)
		blc.timeout = 10
		blc.rotation = RotateRoundRobin
		go blc.worker()
		blc.init = true
		blc.Add(1)
	}
	blc.Unlock()
	return &blc.DB
}

func (blc *Balancer) Close() error {
	close(blc.close)
	blc.Wait()
	return nil
}

func (blc *Balancer) SetRotation(rotation string) {
	blc.Lock()
	switch rotation {
	case "round robin":
		blc.rotation = RotateRoundRobin
	case "use last":
		blc.rotation = RotateUseLast
	case "on fail":
		blc.rotation = RotateOnFail
	}
	blc.Unlock()
}


func (blc *Balancer) worker() {
	defer func() {
		for _, db := range blc.db {
			_ = db.Close()
		}
		blc.Done()
	}()
	for {
		select {
		case <-blc.close:
			return
		case <-time.After(time.Second * time.Duration(blc.timeout)):
			for index, db := range blc.db {
				err := db.Ping()
				blc.Lock()
				blc.state[index] = err == nil
				blc.Unlock()
			}
		}
	}
}

func (blc *Balancer) AddDatabase(db *DB, connect string) {
	err := db.Connect(connect)
	blc.Lock()
	blc.db = append(blc.db, db)
	blc.state = append(blc.state, err == nil)
	blc.Unlock()
}

func (blc *Balancer) CustomQuery(query string, params ...interface{}) Result {
	ctx := context.Background()
	return blc.CustomQueryContext(ctx, query, params...)
}

func (blc *Balancer) CustomQueryContext(ctx context.Context, query string, params ...interface{}) Result {
	result := Result{Headers: make([]TableHeader, 0), Rows: make([]TableData, 0)}
	if db, err := blc.getValidInstance(); err != nil {
		result.Error = err
		return result
	} else {
		return db.QueryContext(ctx, query, params...)
	}
}

func (blc *Balancer) CustomExec(query string, params ...interface{}) ExecResult {
	ctx := context.Background()
	return blc.CustomExecContext(ctx, query, params...)
}

func (blc *Balancer) CustomExecContext(ctx context.Context, query string, params ...interface{}) ExecResult {
	result := ExecResult{}
	if db, err := blc.getValidInstance(); err != nil {
		result.Error = err
		return result
	} else {
		return db.ExecContext(ctx, query, params...)
	}
}

func (blc *Balancer) getValidInstance() (*DB, error) {
	var err = errors.New("no valid database in rotation")

	blc.RLock()
	defer blc.RUnlock()
	switch blc.rotation {
	case RotateRoundRobin:
		if blc.nextWithLock() {
			err = nil
		}
	case RotateUseLast:
		if !blc.state[blc.last] {
			if blc.nextWithLock() {
				err = nil
			}
		} else {
			err = nil
		}
	case RotateOnFail:
		for index, state := range blc.state {
			if state {
				err = nil
				blc.setLastWithLock(index)
				break
			}
		}
	}
	if err != nil {
		return nil, err
	}
	return blc.db[blc.last], err
}

func (blc *Balancer) setLastWithLock(index int) {
	blc.RUnlock()
	blc.setLast(index)
	blc.RLock()
}

func (blc *Balancer) setLast(index int) {
	blc.Lock()
	blc.last = index
	blc.Unlock()
}

func (blc *Balancer) nextWithLock() bool {
	blc.RUnlock()
	defer blc.RLock()
	return blc.next()
}

func (blc *Balancer) next() bool {
	blc.Lock()
	defer blc.Unlock()

	for i, index := 0, blc.last; i < len(blc.state); i++ {
		index++
		if index >= len(blc.state) {
			index = 0
		}
		if blc.state[index] {
			blc.last = index
			return true
		}
	}
	return false
}
