package database

import (
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

type sqliteTestBox struct {
	DB
}

func (db *sqliteTestBox) Init() *DB {
	db.DB.SetDriver("sqlite3")
	db.DB.SetParent(db)
	return &db.DB
}

type invalidTestBox struct {
	DB
}

func (db *invalidTestBox) Init() *DB {
	db.DB.SetDriver("zopa")
	db.DB.SetParent(db)
	return &db.DB
}

func TestBox_New(t *testing.T) {
	box := Box{}
	require.Nil(t, box.Pool, "Invalid create box")
	require.Nil(t, box.update, "Invalid create box")
	box.New()
	require.NotNil(t, box.Pool, "Invalid initialization")
	require.NotNil(t, box.update, "Invalid initialization")
	require.Equal(t, 0, len(box.Pool), "Invalid initialization")
}

func TestBox_Add(t *testing.T) {
	box := Box{}
	db := &sqliteTestBox{}
	db1, err := box.Add(db, "test/sqlite.db", 10, 1)
	require.NoError(t, err, "Invalid add element")
	require.Equal(t, 1, len(box.Pool), "Invalid add element")
	require.Equal(t, &db.parent, &db1.parent, "Invalid address element")

	//	Add existing database
	db2, err := box.Add(db, "test/sqlite.db", 10, 1)
	require.NoError(t, err, "Invalid add element")
	require.Equal(t, 1, len(box.Pool), "Invalid add element")
	require.Equal(t, db1, db2, "Invalid address element")

	//	Add invalid database
	dbInvalid := &invalidTestBox{}
	db3, err := box.Add(dbInvalid, "invalid connection string", 10, 1)
	require.Error(t, err, "Incorrect add invalid element")
	require.Equal(t, 1, len(box.Pool), "Incorrect add invalid element")
	require.Nil(t, db3, "Incorrect add invalid element")
}

func TestBox_Delete(t *testing.T) {
	box := Box{}
	db := &sqliteTestBox{}
	_, err := box.Add(db, "test/sqlite.db", 10, 1)
	require.NoError(t, err, "Invalid add element")
	box.Delete("test/sqlite.db")
	require.Equal(t, 0, len(box.Pool), "Invalid delete element")
}

func TestBox_Element(t *testing.T) {
	box := Box{}
	db := &sqliteTestBox{}
	_, _ = box.Add(db, "test/sqlite.db", 10, 1)
	elem := box.Element("fail connection string")
	require.Nil(t, elem, "Invalid detect fail element")
	elem = box.Element("test/sqlite.db")
	require.NotNil(t, elem, "Invalid get element")
	require.Equal(t, &db.parent, &elem.db.parent, "Invalid address element")
}

func TestBox_Database(t *testing.T) {
	box := Box{}
	db := &sqliteTestBox{}
	_, _ = box.Add(db, "test/sqlite.db", 10, 1)
	db1 := box.Database("test/sqlite.db")
	require.NotNil(t, db1, "Invalid get element")
	require.Equal(t, &db.parent, &db1.parent, "Invalid address element")
}

func TestBox_Status(t *testing.T) {
	box := Box{}
	db := &sqliteTestBox{}
	//	With status checker
	_, _ = box.Add(db, "test/sqlite.db", 10, 1)
	elem := box.Element("test/sqlite.db")
	require.True(t, box.Status("test/sqlite.db"), "Invalid status element")
	elem.status = false
	require.False(t, box.Status("test/sqlite.db"), "Invalid status element")
	time.Sleep(2 * time.Second)
	require.True(t, box.Status("test/sqlite.db"), "Invalid status element")
	box.Delete("test/sqlite.db")
	require.Equal(t, 0, len(box.Pool), "Invalid delete element")

	//	Unknown element
	require.False(t, box.Status("test/sqlite.db"), "Invalid status element")

	//	Without status checker
	_, _ = box.Add(db, "test/sqlite.db", 10, 0)
	elem = box.Element("test/sqlite.db")
	require.True(t, box.Status("test/sqlite.db"), "Invalid status element")
	elem.status = false
	require.False(t, box.Status("test/sqlite.db"), "Invalid status element")
	time.Sleep(2 * time.Second)
	require.False(t, box.Status("test/sqlite.db"), "Invalid status element")
}

