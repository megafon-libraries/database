package database

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)
func TestBalancer_InitAndClose(t *testing.T) {
	var blc = Balancer{}
	require.Nil(t, blc.state, "Initialize Error")
	_ = blc.Init()
	blc.timeout = 1
	db = DB{}
	db.SetDriver("sqlite3")
	blc.AddDatabase(&db, "test/sqlite.db")
	blc.state[0] = false
	require.NotNil(t, blc.state, "Initialize Error")
	time.Sleep(2 * time.Second)
	require.Equal(t, true, blc.state[0], "Incorrect goroutine database checker")
	result := blc.CustomQuery("select txt from calls where txt = :txt", map[string]interface{}{":txt": "ROW1"})
	require.NoError(t, result.Error, "Incorrect balance selector")
	require.Equal(t, "ROW1", result.Rows[0].Data["txt"], "Incorrect balance selector")
	blc.Close()
}

func TestBalancer_SetRotation(t *testing.T) {
	var blc = Balancer{}
	_ = blc.Init()
	require.Equal(t, RotateRoundRobin, blc.rotation, "Incorrect rotation initialize")
	blc.SetRotation("use last")
	require.Equal(t, RotateUseLast, blc.rotation, "Incorrect rotation")
	blc.SetRotation("on fail")
	require.Equal(t, RotateOnFail, blc.rotation, "Incorrect rotation")
	blc.SetRotation("round robin")
	require.Equal(t, RotateRoundRobin, blc.rotation, "Incorrect rotation")
}

func TestBalancer_RoundRobinRotation(t *testing.T) {
	var blc = Balancer{}
	var db *DB
	var err error
	blc.db = []*DB{{driver:  "test1"}, {driver:  "test2"}, {driver:  "test3"}}
	blc.state = []bool{true, true, true}
	blc.rotation = RotateRoundRobin
	blc.last = 0
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test2", db.driver, "Incorrect Round Robin rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test3", db.driver, "Incorrect Round Robin rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test1", db.driver, "Incorrect Round Robin rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test2", db.driver, "Incorrect Round Robin rotation")
	blc.state[2] = false
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test1", db.driver, "Incorrect Round Robin rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test2", db.driver, "Incorrect Round Robin rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test1", db.driver, "Incorrect Round Robin rotation")
	blc.state[1] = false
	blc.state[2] = true
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test3", db.driver, "Incorrect Round Robin rotation")
	blc.state[0] = false
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Round Robin rotation")
	require.Equal(t, "test3", db.driver, "Incorrect Round Robin rotation")
	blc.state[2] = false
	db, err = blc.getValidInstance()
	require.Error(t, err, "Fail Round Robin rotation error detect")
	result := blc.CustomQuery("select txt from calls where txt = :txt", map[string]interface{}{":txt": "ROW1"})
	require.EqualError(t, result.Error, "no valid database in rotation", "Incorrect balance selector")
}

func TestBalancer_UseLastRotation(t *testing.T) {
	var blc = Balancer{}
	var db *DB
	var err error
	blc.db = []*DB{{driver:  "test1"}, {driver:  "test2"}}
	blc.state = []bool{true, true}
	blc.rotation = RotateUseLast
	blc.last = 0
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Use Last rotation")
	require.Equal(t, "test1", db.driver, "Incorrect Use Last rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Use Last rotation")
	require.Equal(t, "test1", db.driver, "Incorrect Use Last rotation")
	blc.state[0] = false
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Use Last rotation")
	require.Equal(t, "test2", db.driver, "Incorrect Use Last rotation")
	blc.state[0] = true
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail Use Last rotation")
	require.Equal(t, "test2", db.driver, "Incorrect Use Last rotation")
}

func TestBalancer_OnFailRotation(t *testing.T) {
	var blc = Balancer{}
	var db *DB
	var err error
	blc.db = []*DB{{driver:  "test1"}, {driver:  "test2"}}
	blc.state = []bool{true, true}
	blc.rotation = RotateOnFail
	blc.last = 0
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail On Fail rotation")
	require.Equal(t, "test1", db.driver, "Incorrect On Fail rotation")
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail On Fail rotation")
	require.Equal(t, "test1", db.driver, "Incorrect On Fail rotation")
	blc.state[0] = false
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail On Fail rotation")
	require.Equal(t, "test2", db.driver, "Incorrect On Fail rotation")
	blc.state[0] = true
	db, err = blc.getValidInstance()
	require.NoError(t, err, "Fail On Fail rotation")
	require.Equal(t, "test1", db.driver, "Incorrect On Fail rotation")
}
