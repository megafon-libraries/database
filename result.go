package database

import (
	"database/sql"
)

type TableHeader struct {
	Name     string `json:"name"`
	Type     string `json:"type"`
	Size     *int64 `json:"size"`
	Nullable *bool  `json:"nullable"`
}

type TableData struct {
	Data map[string]interface{} `json:"data"`
}

type Result struct {
	Headers []TableHeader `json:"headers"`
	Rows    []TableData   `json:"rows"`
	Error   error         `json:"-"`
}

type ExecResult struct {
	sql.Result
	Error error
}

// RemoveColumn delete column from query result
func (result *Result) RemoveColumn(column string) bool {
	passed := false
	for i := range result.Headers {
		if result.Headers[i].Name == column {
			result.Headers = append(result.Headers[0:i], result.Headers[i+1:]...)
			passed = true
			break
		}
	}
	if passed {
		for i := range result.Rows {
			delete(result.Rows[i].Data, column)
		}
	}
	return passed
}

// RenameColumn rename column from query result
func (result *Result) RenameColumn(oldName string, newName string) bool {
	passed := false
	for i := range result.Headers {
		if result.Headers[i].Name == oldName {
			result.Headers[i].Name = newName
			passed = true
			break
		}
	}
	if passed {
		for i := range result.Rows {
			if value, ok := result.Rows[i].Data[oldName]; ok {
				result.Rows[i].Data[newName] = value
				delete(result.Rows[i].Data, oldName)
			}
		}
	}
	return passed
}

// Copy copying query result
func (result *Result) Copy() Result {
	data := Result{
		Headers: make([]TableHeader, 0, len(result.Headers)),
		Rows:    make([]TableData, 0, len(result.Rows)),
		Error:   nil,
	}
	for _, header := range result.Headers {
		data.Headers = append(data.Headers, header)
	}
	for _, row := range result.Rows {
		newRow := TableData{Data: map[string]interface{}{}}
		for key, value := range row.Data {
			newRow.Data[key] = value
		}
		data.Rows = append(data.Rows, newRow)
	}
	return data
}
